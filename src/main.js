import Vue from 'vue'
import App from './App.vue'
import router from './router'
import firebase from 'firebase/app'
import 'firebase/messaging'
import { store } from './store/store';
Vue.config.productionTip = false

firebase.initializeApp({
  apiKey: 'AIzaSyCnMS00pCVU8Vl4piFJZkmkiZsDGum2xkE',
  authDomain: 'fir-fcm-1678a.firebaseapp.com',
  databaseURL: 'https://fir-fcm-1678a.firebaseio.com',
  projectId: 'fir-fcm-1678a',
  storageBucket: 'fir-fcm-1678a.appspot.com',
  messagingSenderId: '1090719381342',
  appId: '1:1090719381342:web:bc3b6113a2cfe7dcb3c1cf',
  measurementId: 'G-F0Y0EWWDXB'
})

Vue.prototype.$messaging = firebase.messaging()
Vue.prototype.$messaging.usePublicVapidKey('BCDf2InUJiVgDGA_2KTY1l3WV2fivK-v1cFbRZAAoY7pSYM7YUYoIyDykzkw66J5E5mqdRKf0LuqouKprIwqFIs')

navigator.serviceWorker.register(`firebase-messaging-sw.js`)
  .then((registration) => {
    Vue.prototype.$messaging.useServiceWorker(registration)
  }).catch(err => {
    console.log(err)
  })

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
