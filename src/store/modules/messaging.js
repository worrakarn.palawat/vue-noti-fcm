import * as types from '../types'

const state = {
    requestPermission: false,
    token: '',
    showToken: false
}

const getters = {
    [types.REQ_PERMISSION]: state => {
        return state.requestPermission
    },
    [types.GET_TOKEN]: state => {
        return state.token
    },
    [types.SHOW_TOKEN]: state => {
        return state.showToken
    }
}

const mutations = {
    [types.MUTATE_REQ_PERMISSION]: (state) => {
        Notification.requestPermission().then((permission) => {
            if (permission === 'granted') {
              console.log('Notification permission granted.')
              state.requestPermission = false
            } else {
              console.log('Unable to get permission to notify.')
              state.requestPermission = true
            }

        })
    },
    [types.MUTATE_SET_TOKEN]: (state, { vm }) => {
        vm.$messaging.getToken()
            .then(currentToken => {
                state.token = currentToken
                state.showToken = true
            })
            .catch(error => {
                state.showToken = false
                console.log(error)
            })
    }
}

const actions = {
    [types.MUTATE_DELETE_TOKEN]: ({ commit, state }, { vm }) => {
         vm.$messaging.getToken()
            .then(currentToken => {
                vm.$messaging.deleteToken(currentToken)
                    .then(() => {
                        console.log('Token deleted.')
                        state.token = ''
                        state.showToken = false
                        commit(types.MUTATE_SET_TOKEN, { vm })
                    })
                    .catch(error => {
                        console.log('Unable to delete token. ', error)
                    })
            })
            .catch(error => {
                console.log(error)
            })
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}