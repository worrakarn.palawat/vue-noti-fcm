// Getters
export const REQ_PERMISSION = 'messaging/REQ_PERMISSION'
export const GET_TOKEN = 'messaging/GET_TOKEN'
export const SHOW_TOKEN = 'messaging/SHOW_TOKEN'



// Mutations
export const MUTATE_REQ_PERMISSION = 'messaging/MUTATE_REQ_PERMISSION'
export const MUTATE_SET_TOKEN = 'messaging/MUTATE_SET_TOKEN'
export const MUTATE_DELETE_TOKEN = 'messaging/MUTATE_DELETE_TOKEN'


