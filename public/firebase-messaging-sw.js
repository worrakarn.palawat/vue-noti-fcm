importScripts('https://www.gstatic.com/firebasejs/7.15.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.15.0/firebase-messaging.js');

var config = {
    apiKey: 'AIzaSyCnMS00pCVU8Vl4piFJZkmkiZsDGum2xkE',
    authDomain: 'fir-fcm-1678a.firebaseapp.com',
    databaseURL: 'https://fir-fcm-1678a.firebaseio.com',
    projectId: 'fir-fcm-1678a',
    storageBucket: 'fir-fcm-1678a.appspot.com',
    messagingSenderId: '1090719381342',
    appId: '1:1090719381342:web:bc3b6113a2cfe7dcb3c1cf',
    measurementId: 'G-F0Y0EWWDXB'
}
firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
      body: 'Background Message body.'
    };
  
    return self.registration.showNotification(notificationTitle,
      notificationOptions);
});